package foundation.e.pwaplayer.provider

import android.content.ContentProvider
import android.content.ContentUris
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import android.util.Log
import foundation.e.pwaplayer.database.PwaDatabase
import foundation.e.pwaplayer.database.mapToPwa
import foundation.e.pwaplayer.provider.PwaConstants.Companion.TABLE_NAME

// Authority of the Content Provider
const val AUTHORITY = "foundation.e.pwaplayer.provider"

// Code to match for multiple rows query
private const val CODE_PWA_DIR = 1

// Code to match for single row query
private const val CODE_PWA_ITEM = 2

private const val TAG = "PwaProvider"

private val uriMatcher = UriMatcher(UriMatcher.NO_MATCH)
    .apply {
        addURI(
            AUTHORITY,
            TABLE_NAME,
            CODE_PWA_DIR
        )
        addURI(
            AUTHORITY, "$TABLE_NAME/*",
            CODE_PWA_ITEM
        )
    }

class PwaProvider : ContentProvider() {

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        Log.d(TAG, "insert() called with: uri = $uri, values = $values")
        when (uriMatcher.match(uri)) {
            CODE_PWA_DIR -> {
                if (values == null) throw IllegalArgumentException("Null ContentValues: Insert failed $uri")
                val pwa = values.mapToPwa()

                val context = context ?: return null
                val id = PwaDatabase.getInstance(context).pwaDao().insert(pwa)
                if (id < 0) {
                    return null
                }
                val uri = ContentUris.withAppendedId(uri, id)
                notifyListeners(uri)
                return uri
            }
            else -> throw IllegalArgumentException("Invalid URI: Insert failed $uri")
        }
    }

    private fun notifyListeners(uri: Uri) {
        context?.contentResolver?.notifyChange(uri, null)
    }

    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor? {
        Log.d(
            TAG,
            "query() called with: uri = $uri, projection = $projection, selection = $selection, selectionArgs = $selectionArgs, sortOrder = $sortOrder"
        )
        val code = uriMatcher.match(uri)
        val cursor: Cursor
        val context = context ?: return null
        when (code) {
            CODE_PWA_DIR, CODE_PWA_ITEM -> {
                val pwas = PwaDatabase.getInstance(context).pwaDao()
                if (code == CODE_PWA_DIR) {
                    cursor = pwas.getAll()
                } else {
                    cursor = pwas.getById(ContentUris.parseId(uri))
                }
                cursor.setNotificationUri(context.contentResolver, uri)
                return cursor
            }
            else -> throw IllegalArgumentException("Unknown URI")
        }
    }

    override fun onCreate(): Boolean {
        return true
    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int {
        Log.d(
            TAG,
            "update() called with: uri = $uri, values = $values, selection = $selection, selectionArgs = $selectionArgs"
        )
        val context = context ?: return 0
        when (uriMatcher.match(uri)) {
            CODE_PWA_DIR -> {
                throw IllegalArgumentException("Invalid URI: Cannot update without ID $uri")
            }
            CODE_PWA_ITEM -> {
                val pwas = PwaDatabase.getInstance(context).pwaDao()
                if (values == null) return 0

                val pwa = values.mapToPwa()
                pwa.id = ContentUris.parseId(uri)
                val count = pwas.update(pwa)
                if (count > 0) {
                    notifyListeners(uri)
                }
                return count
            }
            else -> throw IllegalArgumentException("Unknown uri: $uri")
        }
    }

    // Delete item from the database
    // Either the id of the row is appended at the end of uri or it can be provided by selectionArgs.
    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        Log.d(
            TAG,
            "delete() called with: uri = $uri, selection = $selection, selectionArgs = ${selectionArgs}"
        )
        val context = context ?: return 0
        val pwas = PwaDatabase.getInstance(context).pwaDao()
        when (uriMatcher.match(uri)) {
            CODE_PWA_ITEM -> {
                val count = pwas.delete(ContentUris.parseId(uri))
                if (count > 0) {
                    notifyListeners(uri)
                }
                return count
            }
            CODE_PWA_DIR -> {
                if (selectionArgs.isNullOrEmpty())
                    throw IllegalArgumentException("Invalid URI: cannot delete without ID $uri")
                else {
                    Log.d(TAG, "delete: ${selectionArgs[0]}")
                    val count = pwas.delete(selectionArgs[0])
                    if (count > 0) {
                        notifyListeners(uri)
                    }
                    return count
                }
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun getType(uri: Uri): String? {
        return when (uriMatcher.match(uri)) {
            CODE_PWA_DIR -> "vnd.android.cursor.dir/$AUTHORITY.$TABLE_NAME"
            CODE_PWA_ITEM -> "vnd.android.cursor.item/$AUTHORITY.$TABLE_NAME"
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }
}