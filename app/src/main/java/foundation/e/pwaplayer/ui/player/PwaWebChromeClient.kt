package foundation.e.apps.pwa

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.webkit.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import foundation.e.pwaplayer.R
import foundation.e.pwaplayer.ui.player.PwaActivity

class PwaWebChromeClient(private val pwaActivity: PwaActivity, private val pwaName: String) : WebChromeClient() {

    private lateinit var geoRequestOrigin: String
    private var geoLocationCallback: GeolocationPermissions.Callback? = null
    private var filePathCallback: ValueCallback<Array<Uri>>? = null

    override fun onGeolocationPermissionsShowPrompt(origin: String, callback: GeolocationPermissions.Callback) {
        super.onGeolocationPermissionsShowPrompt(origin, callback)
        if (ContextCompat.checkSelfPermission(pwaActivity, LOCATION_PERMISSION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(pwaActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {

                AlertDialog.Builder(pwaActivity)
                        .setMessage(String.format(pwaActivity.resources.getString(
                                R.string.location_permission_rationale), pwaName))
                        .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                            dialog.dismiss()
                        }
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            geoRequestOrigin = origin
                            geoLocationCallback = callback
                            ActivityCompat.requestPermissions(pwaActivity, arrayOf(LOCATION_PERMISSION),
                                    REQUEST_LOCATION_PERMISSION)
                        }.show()
            } else {
                geoRequestOrigin = origin
                geoLocationCallback = callback
                ActivityCompat.requestPermissions(pwaActivity, arrayOf(LOCATION_PERMISSION),
                        REQUEST_LOCATION_PERMISSION)
            }
        } else {
            // Permission is already granted, Pass it to the callback.
            callback.invoke(origin, true, true)
        }
    }

    fun locationPermissionGranted() {
        geoLocationCallback?.invoke(geoRequestOrigin, true, true)
    }

    fun locationPermissionDenied() {
        geoLocationCallback?.invoke(geoRequestOrigin, false, false)
        Toast.makeText(pwaActivity, R.string.permission_not_granted, Toast.LENGTH_SHORT).show()
    }

    override fun onPermissionRequest(request: PermissionRequest?) {
        request?.grant(request.resources)
    }

    override fun onShowFileChooser(webView: WebView?,
                                   filePathCallback: ValueCallback<Array<Uri>>?,
                                   fileChooserParams: FileChooserParams?): Boolean {
        openFileInput(filePathCallback, fileChooserParams)
        return true
    }

    private fun openFileInput(callback: ValueCallback<Array<Uri>>?,
                              fileChooserParams: FileChooserParams?) {
        val allowMultiple = fileChooserParams?.mode == FileChooserParams.MODE_OPEN_MULTIPLE
        if (filePathCallback != null) {
            filePathCallback!!.onReceiveValue(null)
        }

        filePathCallback = callback
        var intent = fileChooserParams?.createIntent()

        // Create intent if fileChooserParams Intent is null.
        if (intent == null) {
            intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.type = "*/*"
        }

        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, allowMultiple)
        try {
            pwaActivity.startActivityForResult(Intent.createChooser(intent, "Open with"), REQUEST_FILE_CHOOSER)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
            Toast.makeText(pwaActivity, "Activity can't be resolved", Toast.LENGTH_SHORT).show()
        }
    }

    // Called whenever a file chooser event is completed
    fun onActivityResult(resultCode: Int, data: Intent?) {
        if (filePathCallback != null) {
            val result = FileChooserParams.parseResult(resultCode, data)
            filePathCallback!!.onReceiveValue(result)
            filePathCallback = null // Clear the callback.
        }
    }

    companion object {
        private const val TAG = "PwaWebChromeClient"
        private const val LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION
        const val REQUEST_LOCATION_PERMISSION = 101
        const val REQUEST_FILE_CHOOSER = 102
    }
}