package foundation.e.pwaplayer.ui.home

import android.database.ContentObserver
import android.util.Log

private const val TAG = "PwaObserver"

class PwaObserver(private var homeActivity: HomeActivity?) : ContentObserver(null) {

    override fun onChange(selfChange: Boolean) {
        Log.d(TAG, "onChange() called with: selfChange = $selfChange")
        homeActivity?.refreshData()
    }

    fun clear() {
        homeActivity = null
    }
}