package foundation.e.pwaplayer.database

import android.database.Cursor
import androidx.room.*

@Dao
interface PwaDao {
    @Query("SELECT * FROM pwa")
    fun getAll(): Cursor

    @Query("SELECT * FROM pwa WHERE _id = :id")
    fun getById(id: Long): Cursor

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(pwa: Pwa): Long

    @Query("DELETE FROM pwa WHERE _id = :id")
    fun delete(id: Long): Int

    @Query("DELETE FROM pwa WHERE shortcutId = :shortcutId")
    fun delete(shortcutId: String): Int

    @Update
    fun update(pwa: Pwa): Int

    @Query("SELECT COUNT(*) FROM pwa")
    fun count(): Int

}