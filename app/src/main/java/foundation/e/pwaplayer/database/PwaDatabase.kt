package foundation.e.pwaplayer.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Pwa::class], version = 2)
abstract class PwaDatabase : RoomDatabase() {

    abstract fun pwaDao(): PwaDao

    companion object {
        private var instance: PwaDatabase? = null

        // Name of the database exposed by [PwaProvider]
        private const val DB_NAME = "pwa_provider.db"

        @Synchronized
        fun getInstance(context: Context): PwaDatabase {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context, PwaDatabase::class.java,
                    DB_NAME
                ).build()
            }
            return instance!!
        }

    }
}