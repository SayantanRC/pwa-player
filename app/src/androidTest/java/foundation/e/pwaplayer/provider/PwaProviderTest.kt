package foundation.e.pwaplayer.provider

import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.Matchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@SmallTest
class PwaProviderTest {
    private lateinit var contentResolver: ContentResolver

    @Before
    fun setUp() {
        val context: Context = ApplicationProvider.getApplicationContext()
        //SampleDatabase.switchToInMemory(context)
        contentResolver = context.contentResolver
    }

    @Test
    fun cheese_initiallyEmpty() {
        val cursor: Cursor? = contentResolver.query(
            URI_PWA,
            arrayOf<String>("url"),
            null,
            null,
            null
        )
        assertThat(cursor, notNullValue())
        assertThat(cursor!!.count, `is`(0))
        cursor!!.close()
    }
}